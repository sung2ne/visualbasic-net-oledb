# Visual Basic.NET 과 Access 연동

## 개발환경

* Visual Studio 2008
* Access 2010

## 문제가 생길 경우

* Microsoft Access Database Engine 2010 재배포 가능 패키지 설치
* 실행 환경을 AnyCPU에서 x86으로 변경 후 실행

## 실행방법

 소스 코드의 내용에서 ADO.mdb의 위치를 수정한 후 실행하세요.

```
adoCon.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=C:\Users\319호\ADO.mdb; Password=; User ID="
```

## Form1.vb

* 데이터베이스 연결 테스트

## Form2.vb

###  ExecuteScalar

결과 집합에서 첫 번째 행의 첫 번째 열을 반환

### ExecuteNonQuery

Insert, Delete, Update, Set 과 같은 SQL 구문을 실행 후 영향을 받은 행의 개수를 반환

### ExecuteReader

다수의 행을 반환하는 명령에 사용
