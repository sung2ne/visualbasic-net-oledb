﻿Imports System.Data.OleDb

Public Class Form2

    Private Sub BtnExcuteScalar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnExcuteScalar.Click
        Dim adoCon As New OleDBConnection()
        adoCon.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=C:\Users\319호\ADO.mdb; Password=; User ID="
        adoCon.Open()

        If (adoCon.State = ConnectionState.Open) Then
            Dim cmd As New OleDbCommand("SELECT COUNT(*) FROM Employee", adoCon)
            cmd.CommandType = CommandType.Text
            Dim obj As Object = cmd.ExecuteScalar()
            MessageBox.Show("총 사원 수는 : " & obj.ToString())
            adoCon.Close()
        Else
            MessageBox.Show("DB연결에 실패했습니다.")
        End If
    End Sub

    Private Sub BtnExecuteNonQuery_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnExecuteNonQuery.Click
        Dim adoCon As New OleDbConnection()
        adoCon.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=C:\Users\319호\ADO.mdb; Password=; User ID="
        adoCon.Open()

        If (adoCon.State = ConnectionState.Open) Then
            Dim cmd As New OleDbCommand("INSERT INTO Employee (Id,EName,Dept) VALUES (4,'김현수','전산부')", adoCon)

            Dim iResult As Integer
            iResult = cmd.ExecuteNonQuery()
            MessageBox.Show(iResult & "개 행에 영향을 미쳤습니다.")

            adoCon.Close()
        Else
            MessageBox.Show("DB연결에 실패했습니다.")
        End If
    End Sub

    Private Sub BtnExecuteReader_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnExecuteReader.Click
        Dim adoCon As New OleDbConnection()
        adoCon.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=C:\Users\319호\ADO.mdb; Password=; User ID="
        adoCon.Open()

        If (adoCon.State = ConnectionState.Open) Then
            Dim cmd As New OleDbCommand("SELECT Id, EName, Dept FROM Employee", adoCon)
            Dim Reader As OleDbDataReader = cmd.ExecuteReader()

            While Reader.Read
                MessageBox.Show("사원번호: " & Reader(0) & ", 사원명: " & Reader(1) & ", 부서명: " & Reader(2))
            End While

            adoCon.Close()
        Else
            MessageBox.Show("DB연결에 실패했습니다.")
        End If
    End Sub
End Class