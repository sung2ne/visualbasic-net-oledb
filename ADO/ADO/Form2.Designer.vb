﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form2
    Inherits System.Windows.Forms.Form

    'Form은 Dispose를 재정의하여 구성 요소 목록을 정리합니다.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows Form 디자이너에 필요합니다.
    Private components As System.ComponentModel.IContainer

    '참고: 다음 프로시저는 Windows Form 디자이너에 필요합니다.
    '수정하려면 Windows Form 디자이너를 사용하십시오.  
    '코드 편집기를 사용하여 수정하지 마십시오.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.BtnExcuteScalar = New System.Windows.Forms.Button
        Me.BtnExecuteNonQuery = New System.Windows.Forms.Button
        Me.BtnExecuteReader = New System.Windows.Forms.Button
        Me.Button4 = New System.Windows.Forms.Button
        Me.Button5 = New System.Windows.Forms.Button
        Me.Button6 = New System.Windows.Forms.Button
        Me.Button7 = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'BtnExcuteScalar
        '
        Me.BtnExcuteScalar.Location = New System.Drawing.Point(12, 12)
        Me.BtnExcuteScalar.Name = "BtnExcuteScalar"
        Me.BtnExcuteScalar.Size = New System.Drawing.Size(152, 23)
        Me.BtnExcuteScalar.TabIndex = 0
        Me.BtnExcuteScalar.Text = "ExcuteScalar"
        Me.BtnExcuteScalar.UseVisualStyleBackColor = True
        '
        'BtnExecuteNonQuery
        '
        Me.BtnExecuteNonQuery.Location = New System.Drawing.Point(12, 41)
        Me.BtnExecuteNonQuery.Name = "BtnExecuteNonQuery"
        Me.BtnExecuteNonQuery.Size = New System.Drawing.Size(152, 23)
        Me.BtnExecuteNonQuery.TabIndex = 1
        Me.BtnExecuteNonQuery.Text = "ExecuteNonQuery"
        Me.BtnExecuteNonQuery.UseVisualStyleBackColor = True
        '
        'BtnExecuteReader
        '
        Me.BtnExecuteReader.Location = New System.Drawing.Point(12, 70)
        Me.BtnExecuteReader.Name = "BtnExecuteReader"
        Me.BtnExecuteReader.Size = New System.Drawing.Size(152, 23)
        Me.BtnExecuteReader.TabIndex = 2
        Me.BtnExecuteReader.Text = "ExecuteReader"
        Me.BtnExecuteReader.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(12, 99)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(152, 23)
        Me.Button4.TabIndex = 3
        Me.Button4.Text = "Button4"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(12, 128)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(152, 23)
        Me.Button5.TabIndex = 4
        Me.Button5.Text = "Button5"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(12, 157)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(152, 23)
        Me.Button6.TabIndex = 5
        Me.Button6.Text = "Button6"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(12, 186)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(152, 23)
        Me.Button7.TabIndex = 6
        Me.Button7.Text = "Button7"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'Form2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(178, 220)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.BtnExecuteReader)
        Me.Controls.Add(Me.BtnExecuteNonQuery)
        Me.Controls.Add(Me.BtnExcuteScalar)
        Me.Name = "Form2"
        Me.Text = "Form2"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents BtnExcuteScalar As System.Windows.Forms.Button
    Friend WithEvents BtnExecuteNonQuery As System.Windows.Forms.Button
    Friend WithEvents BtnExecuteReader As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
End Class
